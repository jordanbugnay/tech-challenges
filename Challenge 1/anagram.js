const f = (array) => {
  const anagrams = {};
  array.forEach((word)=>{
      const sortedWord = word.split('').sort().join('');
      if (anagrams[sortedWord]) {
          return anagrams[sortedWord].push(word);
      }
      anagrams[sortedWord] = [word];
  });
  return anagrams;
}