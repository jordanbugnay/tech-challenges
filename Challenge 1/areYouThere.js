const f = (arr, string) => {
  const obj = toObject(arr);
  if(!obj[string])
    return false;
  else
    return true;
}

const toObject = (arr) => {
  var rv = {};
  for (var i = 0; i < arr.length; ++i)
    if (arr[i] !== undefined) rv[arr[i]] = 1;
  return rv;
}