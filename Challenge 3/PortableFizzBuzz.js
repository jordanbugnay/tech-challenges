// flexible rules
const rules = [
  {
    keyword: 'Fizz',
    value: [3]
  },
  {
    keyword: 'Buzz',
    value: [5]
  },
  {
    keyword: 'FizzBuzz',
    value: [3, 5]
  }
]

// flexible condition for checking rules
const arrayModuloCheck = (num, array) => {
  let output = false
  for (let i = 0; i < array.length; ++i) {
    if (num % array[i] === 0) {
      output = true
    } else {
      return (output = false)
    }
  }
  return output
}

const rulesIterator = num => {
  let x = num
  for (let i = 0; i < rules.length; ++i) {
    if (arrayModuloCheck(num, rules[i].value)) {
      x = rules[i].keyword
    }
  }
  return x
}

// actual function
const fizzBuzz = num => {
  let output = ''
  for (let x = 1; x <= num; ++x) {
    output += rulesIterator(x)
  }
  return output
}