const findRage = require('./rage/findRage') // utilized race
const focusRage = require('./rage/focusRage') // utilized sequential parallel
const releaseRage = require('./rage/releaseRage') // utilized finally

const helpers = require('./rage/helpers')

console.log('How to become a super saiyan')

helpers.rageWaterfall([findRage, focusRage, releaseRage]) // sequential
