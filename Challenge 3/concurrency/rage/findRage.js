const helpers = require('./helpers')

module.exports = () => {
  // How to become super saiyan
  const traffic = new Promise((resolve, reject) => {
    setTimeout(resolve, helpers.randomTimeout(), 'the traffic sucks.')
  })

  const government = new Promise((resolve, reject) => {
    setTimeout(resolve, helpers.randomTimeout(), 'the government Sucks.')
  })

  const notSaiyan = new Promise((resolve, reject) => {
    setTimeout(resolve, helpers.randomTimeout(), `you're not a saiyan.`)
  })

  const reasonsToBeAngry = [traffic, government, notSaiyan]
  return Promise.race(reasonsToBeAngry).then(reason => {
    console.log(`1. Get angry because ${reason}`)
  })
}
