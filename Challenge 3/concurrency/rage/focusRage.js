const helpers = require('./helpers')

// Step 2 focus your rage
const prepareBody = new Promise((resolve, reject) => {
  setTimeout(
    resolve,
    helpers.randomTimeout(),
    'Look down at the ground, drop your arms, and begin to focus your rage'
  )
})
const grindTeeth = new Promise((resolve, reject) => {
  setTimeout(
    resolve,
    helpers.randomTimeout(),
    'Start grinding teeth and feel unintentional rage grunting'
  )
})

const breathing = new Promise((resolve, reject) => {
  setTimeout(
    resolve,
    helpers.randomTimeout(),
    'Breathe slowly while flexing every muscle you have'
  )
})
const initialRage = [prepareBody, grindTeeth, breathing]

const initialRageStart = () =>
  Promise.all(initialRage).then(rage => {
    console.log('2. Prepare your body by doing this things all at once')
    rage.forEach(r => {
      console.log(`---> ${r}`)
    })
  })

const feelEyeBlur = new Promise((resolve, reject) => {
  setTimeout(
    resolve,
    helpers.randomTimeout(),
    'Notice a change in your hair color and your vision will look odd'
  )
})

const fixPosture = new Promise((resolve, reject) => {
  setTimeout(
    resolve,
    helpers.randomTimeout(),
    'Notice the ground below crack while tilting your head back and still maintaining that rigid posture.'
  )
})
const secondaryRage = [feelEyeBlur, fixPosture]

const secondaryRageStart = () =>
  Promise.all(secondaryRage).then(rage => {
    console.log(
      `After you've done this things all at once, you'll feel two things at the same time`
    )
    rage.forEach(r => {
      console.log(`---> ${r}`)
    })
  })

module.exports = () =>
  helpers.rageWaterfall([initialRageStart, secondaryRageStart])
