module.exports = {
  rageWaterfall: steps => {
    return steps.reduce(function (accumulator, callback) {
      return accumulator.then(callback)
    }, Promise.resolve())
  },
  randomTimeout: () => Math.random() * 10 + 2
}
