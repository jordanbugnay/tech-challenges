module.exports = () => {
  console.log(`3. Release rage`)
  return new Promise((resolve, reject) => {
    const mentalConditions = [
      `Pay no mind to the stunned spectators around you. They could attack but are usually too mesmerized by your awesomeness to get wise and attack your completely vulnerable state`,
      `Now that your veins are bulging to a point of breaking, yellow energy should begin to envelop your body`,
      `You won't see this as your eyes will be far back into your head at this point.`
    ]
    resolve(mentalConditions)
  })
    .then(conditions => {
      conditions.forEach(c => {
        console.log(`---> ${c}`)
      })
      console.log(`Scream all the rage stored in your body.`)
    })
    .finally(() => {
      let result
      const successRate = Math.random() >= 0.5
      if (successRate) {
        result = `You have so much hatred. Congrats you're now a super saiyan.`
      } else {
        result = `Not enough rage. Think of something awful again`
      }

      console.log(`Result: ${result}`)
    })
}
