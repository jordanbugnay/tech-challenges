"use strict";

function Car(name, beep) {
  this.name = name || "Bat mobile";
  this.beep = this.beeps[name.toLowerCase()] || "na na na na na na na na na";
}

Car.prototype.beep = function() {
  console.log(this.beep);
};

Car.prototype.beeps = {
  ciaz: "a",
  city: "b",
  vios: "c",
  rio: "d"
};
