class Car {
  constructor(name = "Bat mobile", beep = "na na na na na") {
    this.name = name;
    this.beep = this.getBeep(name.toLowerCase()) || beep;
  }

  getBeep(car) {
    const cars = {
      ciaz: "a",
      city: "b",
      vios: "c",
      rio: "d"
    };
    return cars[car];
  }

  beep() {
    console.log(this.beep);
  }
}
