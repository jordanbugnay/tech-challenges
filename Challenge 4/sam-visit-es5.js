const obj = {
    name: "Sam",
    placesVisited: ["Manila", "Pasig", "Mandaluyong", "Taguig"], 
    f: function () {
        var name = this.name
        this.placesVisited.forEach(function(place) {
            console.log(name + ' has visited ' + place)
        });
    }
  }

  obj.f()