const obj = {
    name: "Sam",
    placesVisited: ["Manila", "Pasig", "Mandaluyong", "Taguig"], 
    f: () => {
        obj.placesVisited.forEach((place) => console.log(`${obj.name} has visited ${place}`));
    }
  }

  obj.f()