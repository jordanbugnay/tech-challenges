
DROP TABLE tech_challenge.whitelists;
DROP TABLE tech_challenge.blacklists;

DROP SCHEMA tech_challenge;

CREATE SCHEMA tech_challenge


CREATE TABLE tech_challenge.whitelists (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    url TEXT
);

CREATE INDEX whitelist_url_idx ON tech_challenge.whitelists (url);
CREATE INDEX ON tech_challenge.whitelists ((lower(url)));

CREATE TABLE tech_challenge.blacklists (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    url TEXT
);
CREATE INDEX blacklist_url_idx ON tech_challenge.blacklists (url);
CREATE INDEX ON tech_challenge.blacklists ((lower(url)));


INSERT INTO tech_challenge.whitelists (name, url) 
    VALUES ('a', 'a.com'),('b', 'b.com'),('c', 'c.com'), ('d', '');

INSERT INTO tech_challenge.blacklists (name, url) 
    VALUES ('a', 'a.com'),('b1', 'b.com'),('d', 'd.com');