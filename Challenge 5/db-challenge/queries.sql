/**
a. Give the names of records present in both tables (w/ the same URLs)
*/

SELECT wl.name AS common_names
FROM tech_challenge.whitelists AS wl
    INNER JOIN tech_challenge.blacklists AS bl ON lower(wl.url) = lower(bl.url);


/**
b. Give the names of all blacklisted URLs that are not in the whitelists table
*/

SELECT bl.name AS blacklists
FROM tech_challenge.blacklists AS bl
WHERE lower(url) NOT IN (SELECT lower(url)
FROM tech_challenge.whitelists
WHERE url IS NOT NULL AND name IS NOT NULL);


/**
c. Give the names of all blacklists and all whitelists
*/

/** without duplicates */
    SELECT name
    FROM tech_challenge.whitelists
UNION
    SELECT name
    FROM tech_challenge.blacklists;
/** with duplicates */
    SELECT name
    FROM tech_challenge.whitelists
UNION ALL
    SELECT name
    FROM tech_challenge.blacklists;

/**
d. Give the names of all blacklists, and all whitelists that are also in blacklists.
*/

    SELECT name
    FROM tech_challenge.blacklists as bl LEFT OUTER JOIN tech_challenge.whitelists as wl
        ON lower(bl.url) = lower(wl.url)
UNION ALL
    SELECT name
    FROM tech_challenge.blacklists; 
