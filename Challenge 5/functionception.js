function obj(x) {
  function second(y) {
    function third(z) {
      return `${x} ${y} ${z}`;
    }
    return third;
  }
  return second;
}

console.log(obj("Sam")("is")("here"));

// extra

function SAMmation(num) {
  let result = num;
  function argsChecker(additionalArgs) {
    if (additionalArgs !== undefined) {
      result += ` ${additionalArgs}`;
      return argsChecker;
    } else {
      return result;
    }
  }
  return argsChecker;
}

console.log(SAMmation("Sam")("is")("here")("or")("is")("he")("?")());
console.log(SAMmation("Sam")());
