
DROP TABLE tech_challenge.users;
DROP TABLE tech_challenge.user_addresses;
DROP TABLE tech_challenge.user_pets;

DROP SCHEMA tech_challenge;

CREATE SCHEMA tech_challenge


CREATE TABLE tech_challenge.users (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    age INT
);

CREATE INDEX users_name_idx ON tech_challenge.users (name);

CREATE TABLE tech_challenge.user_addresses (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    user_id BIGSERIAL NOT NULL,
    country TEXT
);
CREATE INDEX users_addresses_country_idx ON tech_challenge.user_addresses (country);

CREATE TABLE tech_challenge.user_pets (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    user_id BIGSERIAL NOT NULL,
    name TEXT
);
CREATE INDEX users_pets_name_idx ON tech_challenge.user_pets (name);


INSERT INTO tech_challenge.users (name, age) 
    VALUES ('ash', 10),('brock', 11),('misty', 9);

INSERT INTO tech_challenge.user_addresses (user_id, country) 
    VALUES ((SELECT id FROM tech_challenge.users WHERE name = 'ash'), 'PH'),
            ((SELECT id FROM tech_challenge.users WHERE name = 'brock'), 'Makati, PH'),
            ((SELECT id FROM tech_challenge.users WHERE name = 'misty'), 'US');

INSERT INTO tech_challenge.user_pets (user_id, name) 
VALUES ((SELECT id FROM tech_challenge.users WHERE name = 'ash'), 'Pikachu'),
        ((SELECT id FROM tech_challenge.users WHERE name = 'ash'), 'Charizard'),
        ((SELECT id FROM tech_challenge.users WHERE name = 'brock'), 'Onix'),
        ((SELECT id FROM tech_challenge.users WHERE name = 'misty'), 'Psyduck');