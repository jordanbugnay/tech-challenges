/**
a. Get user name, age, and their number of pets.
*/

SELECT u.name, u.age, COUNT(1) AS number_of_pets
FROM
    tech_challenge.users AS u INNER JOIN tech_challenge.user_pets AS up
    ON u.id = up.user_id
GROUP BY u.name, u.age;

/**
b. Make the name of pet with id 1 the same as its user's name
*/

UPDATE tech_challenge.user_pets SET name = 
(SELECT u.name
FROM
    tech_challenge.users AS u INNER JOIN tech_challenge.user_pets AS up
    ON u.id = up.user_id
WHERE up.id = 1)
WHERE tech_challenge.user_pets.id = 1;

/**
c. Make all pet names the same as their user's names
*/

