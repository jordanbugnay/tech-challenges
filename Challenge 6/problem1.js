class Person {
  setName(name) {
    this.name = name;
    return this;
  }

  setAge(age) {
    this.age = age;
    return this;
  }

  setAddress(address) {
    this.address = address;
    return this;
  }
}
