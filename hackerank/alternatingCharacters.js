const s = "AAA";
function alternatingCharacters(s) {
  let numDel = 0;
  let base = "";
  const arr = s.split("");

  for (let i = 0; i < arr.length - 1; i++) {
    base = arr[i];
    if (base === arr[i + 1]) {
      numDel++;
    } else {
      base = arr[i];
    }
  }
  return numDel;
}

console.log(alternatingCharacters(s));
