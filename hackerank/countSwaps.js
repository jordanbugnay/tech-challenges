const a = [3, 2, 1];

function countSwaps(a) {
  let swapCount = 0;
  const n = a.length - 1;
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < n; j++) {
      if (a[j] > a[j + 1]) {
        let temp = a[j];
        a[j] = a[j + 1];
        a[j + 1] = temp;
        swapCount++;
      }
    }
  }

  let result = `Array is sorted in ${swapCount} swaps.`;
  result += `\nFirst Element: ${a[0]}`;
  result += `\nLast Element: ${a[a.length - 1]}`;

  return result;
}

console.log(countSwaps(a));
