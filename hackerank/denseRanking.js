const scores = [100, 90, 90, 80, 75, 60];
const alice = [50, 65, 77, 90, 102];

function denseRanking(scores, alice) {
  const ranking = Array.from(new Set(scores));
  const aliceRanking = [];
  alice.forEach(aScore => {
    let currentRanking = ranking.slice(0);
    currentRanking.push(aScore);
    currentRanking = Array.from(new Set(currentRanking));
    currentRanking = currentRanking.sort((a, b) => b - a);
    currentRanking.some((cr, index) => {
      console.log(cr);
      if (cr === aScore) {
        return aliceRanking.push(index + 1);
      }
    });
  });
  let result = "";
  aliceRanking.forEach(rank => {
    return (result += rank + "\n");
  });
  return result.trim();
}

console.log(denseRanking(scores, alice));
