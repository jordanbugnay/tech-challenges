const arr = [
  [1, 1, 1, 0, 0, 0],
  [0, 1, 0, 0, 0, 0],
  [1, 1, 1, 0, 0, 0],
  [0, 0, 2, 4, 4, 0],
  [0, 0, 0, 2, 0, 0],
  [0, 0, 1, 2, 4, 0]
];

function hourglassSum(arr) {
  let sums = [];
  for (let i = 0; i < 4; i++) {
    for (let j = 0; j < 4; j++) {
      const top = getSumOfBases(arr[i], j);
      const mid = getMid(arr[i + 1], j);
      const bottom = getSumOfBases(arr[i + 2], j);
      const hourglass = [top, mid, bottom];
      const summation = getSummation(hourglass);
      sums.push(summation);
    }
  }
  return sums[sums.indexOf(Math.max(...sums))];
}

function getSumOfBases(arr, position) {
  const base = arr.slice(position, position + 3);
  const summation = getSummation(base);
  return summation;
}

function getSummation(arr) {
  return arr.map(num => parseInt(num)).reduce((a, b) => a + b, 0);
}

function getMid(arr, position) {
  return arr.slice(position + 1, position + 2).join("");
}

console.log(hourglassSum(arr));
