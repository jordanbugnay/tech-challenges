const a = "fsqoiaidfaukvngpsugszsnseskicpejjvytviya";
const b = "ksmfgsxamduovigbasjchnoskolfwjhgetnmnkmcphqmpwnrrwtymjtwxget";

function makeAnagram(a, b) {
  let deleteCount = 0;
  let aSubstract = 0;
  let anagramCount = 0;

  let baseArray = a.split("");
  let array = b.split("");
  if (baseArray.length !== array.length) {
    baseArray = a.length < b.length ? a.split("") : b.split("");
    array = a.length > b.length ? a.split("") : b.split("");
  }

  const hashtable = baseArray.reduce((map, obj) => {
    if (map[obj]) {
      map[obj] = map[obj] + 1;
    } else {
      map[obj] = 1;
    }
    return map;
  }, {});
  array.forEach(element => {
    if (hashtable[element]) {
      anagramCount++;
      hashtable[element] = hashtable[element] - 1;
    }
  });
  deleteCount = a.length + b.length - anagramCount * 2;

  return deleteCount;
}

console.log(makeAnagram(a, b));
