const ar = [1, 2, 3, 3];

function maxCount(ar) {
  let count = 0;
  const max = Math.max(...ar);
  ar.forEach(element => {
    if (element === max) {
      count++;
    }
  });
  return count;
}

console.log(maxCount(ar));
