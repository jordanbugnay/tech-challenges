const prices = [1, 12, 5, 111, 200, 1000, 10];
const k = 50;

function maximumToys(prices, k) {
  prices = prices.sort((a, b) => a - b);
  let expense = 0;
  let numberOfToys = 0;
  for (let i = 0; i <= prices.length - 1; i++) {
    expense += prices[i];
    if (expense <= k) {
      numberOfToys++;
    } else {
      break;
    }
  }
  return numberOfToys;
}

console.log(maximumToys(prices, k));
