let arr = [1, 2, 5, 4, 3];

function minMax(arr) {
  let sortedArray = arr.sort();
  let minArray = arr.slice(0);
  minArray.pop();
  let smallest = minArray.reduce((x = 0, num) => (x += num));
  let maxArray = arr;
  maxArray.shift();
  let largest = maxArray.reduce((x = 0, num) => (x += num));

  return `${smallest} ${largest}`;
}

console.log(minMax(arr));
