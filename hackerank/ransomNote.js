const magazine = ["give", "me", "one", "grand", "today", "night"];
const note = ["give", "one", "grand", "today"];

function checkMagazine(magazine, note) {
  let magazineHT = magazine.reduce((map, m) => {
    let count = 1;
    if (map[m]) {
      count++;
    }
    map[m] = count;
    return map;
  }, {});
  let result = "Yes";

  for (let i = 0; i < note.length; i++) {
    if (magazineHT[note[i]] === undefined || magazineHT[note[i]] < 1) {
      result = "No";
      break;
    } else {
      const value = magazineHT[note[i]];
      magazineHT[note[i]] = value - 1;
    }
  }
  console.log(magazineHT);
  return result;
}

function checkMagazine2(magazine, note) {
  var arr = [];
  var dict = {};
  for (var i = 0; i < magazine.length; i++) {
    if (dict[magazine[i]]) {
      dict[magazine[i]] += 1;
    } else {
      dict[magazine[i]] = 1;
    }
  }
  for (var i = 0; i < note.length; i++) {
    if (dict[note[i]] && dict[note[i]] > 0) {
      dict[note[i]]--;
      arr.push(true);
    }
  }
  console.log(arr.length === note.length ? "Yes" : "No");
}

console.log(checkMagazine2(magazine, note));
