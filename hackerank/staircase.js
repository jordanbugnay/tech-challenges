function staircase(n) {
  let result = "";
  for (let i = n; i > 0; i--) {
    let container = new Array(n);
    container = container
      .fill(" ")
      .fill("#", i - 1, container.length)
      .join("");
    result += container;
    if (i != 1) {
      result += "\n";
    }
  }
  return result;
}

console.log(staircase(6));
