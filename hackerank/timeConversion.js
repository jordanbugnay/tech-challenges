function timeConversion(s) {
  let timeArray = s.split(":");
  const timeSuffix = timeArray[2].slice(-2);
  let hour = timeArray[0];

  timeArray[2] = timeArray[2].slice(0, 2);

  if (timeSuffix === "PM") {
    if (parseInt(hour) !== 12) {
      timeArray[0] = parseInt(hour) + 12;
    }
  } else {
    if (hour == 12) {
      timeArray[0] = "00";
    }
  }

  return timeArray.join(":");
}

console.log(timeConversion("12:05:45PM"));
