const a = [1, 2, 3, 4, 5];
const d = 4;

function rotLeft(a, d) {
  let result = a;
  for (let i = 0; i < d; i++) {
    result.push(result.shift());
  }
  return result;
}

console.log(rotLeft(a, d));
