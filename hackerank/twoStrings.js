const s1 = "plaedfldlypxrbihxfwlqjzfrfecmsvzbwvro";
const s2 = "spgvwiduxafqedjwilgxicczylhvgibmllujh";

function twoStrings(s1, s2) {
  let result = "NO";
  let s = "";
  let sArr = "";
  if (s1.length === s2.length) {
    s = s1;
    sArr = s2;
  } else {
    s = s1.length > s2.length ? s1 : s2;
    sArr = s1.length < s2.length ? s1 : s2;
  }

  s = uniq_fast(s.split(""));
  sArr = uniq_fast(sArr.split(""));

  console.log(s);
  console.log(sArr);
  var dict = {};
  for (var i = 0; i < s.length; i++) {
    if (dict[s[i]]) {
      dict[s[i]] += 1;
    } else {
      dict[s[i]] = 1;
    }
  }

  for (let i = 0; i < sArr.length; i++) {
    if (dict[sArr[i]]) {
      result = "YES";
      break;
    }
  }

  return result;
}

function uniq_fast(a) {
  var seen = {};
  var out = [];
  var len = a.length;
  var j = 0;
  for (var i = 0; i < len; i++) {
    var item = a[i];
    if (seen[item] !== 1) {
      seen[item] = 1;
      out[j++] = item;
    }
  }
  return out;
}

console.log(twoStrings(s1, s2));
