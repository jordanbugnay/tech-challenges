function printPalinPrimes(N) {
  let err = "Please enter a number";
  if (isNaN(parseInt(N))) {
    return err;
  }
  if (typeof parseInt(N) !== "number") {
    return err;
  }

  let result = "";
  let divisor = 2;
  if (N < 2) {
    return result;
  }
  if (N === 2) {
    return (result = 2);
  }

  for (let i = 0; i <= N; i++) {
    const isPrime = checkIfPrime(i);
    if (isPrime) {
      if (i <= 9) {
        result += `${i},`;
      } else {
        const isPalindrome = checkIfNumberPalindrome(i);
        if (isPalindrome) {
          result += `${i},`;
        }
      }
    }
  }

  return result.slice(0, -1);
}

function checkIfPrime(num) {
  if (num === 2) {
    return 2;
  }

  let limit = num / 2;

  for (var i = 2; i < limit; i++) {
    if (num % i === 0) {
      return false;
    }
  }
  return num;
}

function checkIfNumberPalindrome(num) {
  let container = `${num}`.split("");
  const limit = Math.floor(container.length / 2);
  for (let i = 0; i < limit; ++i) {
    if (container[i] !== container[container.length - i - 1]) {
      return false;
    }
  }

  return true;
}

console.log(printPalinPrimes(1000));
// 1, 2, 3, 5, 7, 9, 11
