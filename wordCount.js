function wordCount(sentence) {
  let count = 0;
  if (typeof sentence !== "string") {
    return count;
  }

  const wordChecker = sentence.split(" ");
  wordChecker.forEach(element => {
    if (element) {
      count++;
    }
  });
  return count;
}

console.log(wordCount(1));
// 6

// function wordCount2(sentence) {
//   let count = 1;
//   let wordChecker = sentence.split(" ");
//   let flag = false;

//   return wordChecker;
// }
// console.log("2", wordCount2("The quick brown  fox is quick p"));
